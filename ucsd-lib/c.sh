#!/bin/sh

usage() {
  echo "$0 <bag.json>"
}

if [ $# -ne 1 ]; then
  usage
  exit 1
fi
bagjson=$1
if [ ! -f $bagjson ]; then
  echo "missing $bagjson"
  exit 1
fi

#curl -X POST -H "Content-Type: application/json" -u admin:XXXX --data @$bagjson http://localhost:8080/api/bags
#curl --insecure -X POST -H "Content-Type: application/json" -u admin:XXXX --data @$bagjson https://localhost:8443/api/bags
curl --insecure -X POST -H "Content-Type: application/json" -u admin:XXXX --data @$bagjson https://localhost:8443/api/bags
