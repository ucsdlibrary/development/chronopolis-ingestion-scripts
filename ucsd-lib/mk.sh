#!/bin/sh

DEPOSITOR=ucsd-lib
COLLECTIONS=collections
SIZE=???
TOTALFILES=???
STORAGEREGION=1
TEMPLATE=curl.json.template
BAGS=/export/outgoing/bags

if [ ! -f $COLLECTIONS ]; then
  echo "missing $COLLECTIONS"
  exit 1
fi

if [ ! -f $TEMPLATE ]; then
  echo "missing $TEMPLATE"
  exit 1
fi

echo -n "start "
date

while read collection; do
  echo $collection
  src=$BAGS/$DEPOSITOR/$collection
  if [ ! -d $src ]; then
    echo "missing $src"
    continue
  fi
  SIZE=`find $src -type f -printf "%s\n" | awk '{sum+=$1}END{print sum}'`
  TOTALFILES=`find $src -type f -print | wc -l`
  sed "
    s/COLLECTION/$collection/g
    s/DEPOSITOR/$DEPOSITOR/g
    s/SIZE/$SIZE/g
    s/TOTALFILES/$TOTALFILES/g
    s/STORAGEREGION/$STORAGEREGION/g
  " $TEMPLATE > curl-$collection.json
done < $COLLECTIONS

echo -n "end "
date
