#!/bin/sh

cmd=./c.sh

if [ ! -f $cmd ]; then
  echo "missing $cmd"
  exit 1
fi

if [ ! -d succeed ]; then
  echo "missing directory succeed"
  exit 1
fi

for f in curl-*; do
  echo $f
  $cmd $f
  if [ $? -eq 0 ]; then
    echo "succeed $f"
    mv $f succeed
  else
    echo "fail $f"
    mv $f fail
  fi
done
