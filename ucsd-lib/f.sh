#!/bin/sh

# fixity (sha356) of tagmanifest

DEPOSITOR=ucsd-lib
COLLECTIONS=collections
TEMPLATE=fixity.json.template
BAGS=/export/outgoing/bags
BAGIDS=bagids

if [ ! -f $COLLECTIONS ]; then
  echo "missing $COLLECTIONS"
  exit 1
fi
if [ ! -f $TEMPLATE ]; then
  echo "missing $TEMPLATE"
  exit 1
fi
if [ ! -f $BAGIDS ]; then
  echo "missing $BAGIDS"
  exit 1
fi

#curl --insecure -X PUT -H "Content-Type: application/json" -u admin:XXXX --data @$bagjson https://localhost:8443/api/bags/$bagid/storage/BAG/fixity

while read collection; do
#  echo $collection
  tagmanifest=$BAGS/$DEPOSITOR/$collection/tagmanifest-sha256.txt
  if [ ! -f $tagmanifest ]; then
    echo "missing $tagmanifest"
    continue
  fi
  fixity=`sha256sum $tagmanifest | sed 's/ .*$//'`
  bagid=`grep $collection $BAGIDS | sed 's/^.* //'`
  if ! expr "$bagid" : "^[0-9][0-9]*$" >/dev/null; then
    echo "not a bagid $collection $bagid"
    continue
  fi
  fixityjson=$bagid.fixity.json
  if [ -f $fixityjson ]; then
    echo "already exists $fixityjson"
    continue
  fi
  if ! cp $TEMPLATE $fixityjson; then
    exit 1
  fi
  sed -i "s/FIXITY/$fixity/" $fixityjson
  echo "$collection $fixity $bagid"
  curl --insecure -X PUT -H "Content-Type: application/json" -u admin:XXXX --data @$fixityjson https://localhost:8443/api/bags/$bagid/storage/BAG/fixity
#  echo "curl --insecure -X PUT -H \"Content-Type: application/json\" -u admin:NOTPASSWORD --data @$fixityjson https://localhost:8443/api/bags/$bagid/storage/BAG/fixity"
done < $COLLECTIONS
