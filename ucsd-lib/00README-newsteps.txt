Newest deposit/ingest requires new attributes in curl and some manual steps

deposit curl new attributes
     "size": 609,
     "totalFiles": 5,
     "storageRegion": 1,

fixity needs sha256 of tagmanifest
sha256sum /export/outgoing/bags/rontest/test7/tagmanifest-sha256.txt

vi fixity.json

fixity.json
{
    "algorithm": "sha256",
    "value": "7f5800c2ffa0f8fde42b824d6251949ff9abac3a6836ef8d7ab9e7cb412532ae"
}

vi f.sh
  Need bag ID in curl URL
  Obtain from deposit curl output or UI
  curl --insecure -X PUT -H "Content-Type: application/json" -u admin:admin --data @fixity.json https://localhost:8443/api/bags/52320/storage/BAG/fixity

./f.sh fixity.json

Lastly, manually tokenize
cd ~/tokenizer
  java -jar tokenizer.jar

