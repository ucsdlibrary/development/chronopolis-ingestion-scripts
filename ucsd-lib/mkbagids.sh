#!/bin/sh

runout=run.out

if [ ! -f $runout ]; then
  echo "missing $runout"
  exit 1
fi

sed -n '
  /^curl-/{s/curl-//;s/.json//;h;n}
  /"id"/{s/.*: //;s/,//;H;x;s/\n/ /;p}
' $runout

#curl-ucsd-lib_bb0_2018-02-19-17-52-08.json
#  "id" : 13934,
