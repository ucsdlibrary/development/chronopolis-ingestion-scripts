============ Bagging ===============
Perform the following as the user running the ingest service,
otherwise ingest might not be able to read the collection files.
In my case the user is named "chronopolis".

Create a subdirectory for the depositor in the directory specified
as chron.stage.bags in the ingest application.yml.
In my case the directory is "/export/outgoing/bags".

The depositor for this test is "rontest".

cd /export/outgoing/bags
mkdir rontest

Assuming the collection is not yet a bag, but just a directory,
and the collection will have the same name as the directory,
then copy the collection into the outgoing area.
In my case, the collection is in "~/test1" and will be named "test1".

cd rontest
rsync -a ~/test1 .

Bagging will create a default "bag-info.txt" containing bagging date
and size.  If additional attributes are required, create a file
containing them.  This file can be named anything and will be
specified during bagging.  In my case the file is named "bag-info.txt".
vi bag-info.txt
  Organization: Hamsters R Us
  Contact: Ron Stanonik

I'm fond of "baginplace", which moves the contents of the directory
into the payload subdirectory named "data".

Chronopolis requires sha256 for both tagmanifest and payloadmanisfest,
so specify "sha256".

bag baginplace --tagmanifestalgorithm=sha256 --payloadmanifestalgorithm=sha256 test1

The validity of the bag can be checked.

bag verifyvalid test1

If the bag is valid, verifyvalid will report
  Result is true.

Otherwise verifyvalid will report something like the following,
depending upon the error.
  Result is false.
  (error) Tag manifest tagmanifest-sha256.txt contains invalid files: [manifest-sha256.txt]
  Complete results written to /export/outgoing/bags/rontest/test1-verifyvalid-20171201-14:27:59.txt

============ Depositing ===============
Create a json file that will be used to deposit the collection.
The file can be named anything.
Check with Mike about the correct storageRegion, requiredReplications,
and replicatingNodes.
I compute the size and totalFiles
  SIZE=`find test1 -type f -printf "%s\n" | awk '{sum+=$1}END{print sum}'`
  TOTALFILES=`find test1 -type f -print | wc -l`

vi curl-test1.json
{
     "name": "test1",
     "size": 433,
     "totalFiles": 5,
     "storageRegion": 1,
     "location": "rontest/test1",
     "depositor": "rontest",
     "requiredReplications": 2,
     "replicatingNodes": [
         "ucsddev",
         "tdldev"
     ]
}

Deposit the collection.  "user:password" are the user and password of an
account that has permission to deposit.

I run this command on the ingest server, so "localhost" is the ingest
server, but the command can be run on another host, in which case,
the ingest server must be specified, not localhost.

curl --insecure -X POST -H "Content-Type: application/json" -u user:password --data curl-test1.json https://localhost:8443/api/bags

Next the fixity of the tagmanifest must be computed and uploaded.
Get the sha256 and create a json file containing it.

sha256sum test1/tagmanifest-sha256.txt

vi fixity.json
{
    "algorithm": "sha256",
    "value": "7f5800c2ffa0f8fde42b824d6251949ff9abac3a6836ef8d7ab9e7cb412532ae"
}

The bag ID is required and can be obtained from the output of previous curl
or from the UI for the bag.  In this case the bag ID is 52320.

curl --insecure -X PUT -H "Content-Type: application/json" -u user:password --data @fixity.json https://localhost:8443/api/bags/52320/storage/BAG/fixity

============ Tokenize ===============
The last step is to tokenize the collection.
Mike is working on having this happen automatically, but for now
it is performed manually.
This step requires downloading the tokenizer.jar.

java -jar tokenizer.jar

============ Monitor Progress ===============
Watch the ingest logs and/or ingest UI.

tail -f /var/log/chronopolis/ingest.log

In the ingest UI, the bag status will change
  DEPOSITED
  INITIALIZED
  TOKENIZED
  REPLICATING
  PRESERVED
