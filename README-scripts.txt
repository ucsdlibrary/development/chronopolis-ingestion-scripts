Chronopolis Ingest Process

Assumes bagging already done

===================

## Steps

1. copy bag to staging area
2. create json
3. stage
4. create fixity json (sha256 of tag-manifest-sha256.txt)
5. add fixity
6. tokenize

===================

Copy bag to staging area on chron-ingest

/export/outgoing/bag/<depositor>/<bag>

For example
/export/outgoing/bag/ucsd-lib/ucsd-lib_bb0_2017-10-09-10-02-43

NOTE: The bag name starts with the depositor and the timestamp includes
hours-minutes-seconds.

===================

cd /usr/lib/chronopolis/ucsd-lib

vi collections
ucsd-lib_bb0_2017-10-09-10-02-43
ucsd-lib_bb1_2017-10-09-10-02-43
...

vi mk.sh
DEPOSITOR=ucsd-lib

curl.template.json
{
     "name": "COLLECTION",
     "size": SIZE,
     "totalFiles": TOTALFILES,
     "storageRegion": STORAGEREGION,
     "location": "DEPOSITOR/COLLECTION",
     "depositor": "DEPOSITOR",
}

requiredReplications and requiredNodes are now specified in the UI

./mk.sh
Creates a json for each collection in colletions using curl.template.json

mk.sh will calculate SIZE and TOTALFILES and replace the strings
mk.sh will replace STORAGEREGION with "1"

./run.sh >& run.out

===================

The following reads run.out from the previous step and
extracts the bag name and bagid

./mkbagids.sh > bagids

bagids contains
<bag name> <bagid>
...

===================

The following reads collections, calculates the fixity (sha256 of tagmanifest),
extracts the bagid from bagids, creates <bagid>.fixity.json, and finally curls
the fixity for bagid to chron-ingest.

./f.sh

===================

Tokenize

cd ../tokenizer
java -jar tokenizer.jar
